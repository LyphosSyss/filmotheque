import UtilTab from '../../src/models/UtilTab.js'

test('Fonction de TEST n*1', () => {
    const tab = ['testA', 'testB'];
    expect(UtilTab.toHtmlTab(tab, 2)).toMatch('<td>testA</td>');
    expect(UtilTab.toHtmlTab(tab, 2)).toMatch('<td>testB</td>');
});

test('Fonction de TEST n*2', () => {
    const tab = ['testB', 'testC'];
    expect(UtilTab.toHtmlTab(tab, 2)).toMatch('<td>testB</td>');
    expect(UtilTab.toHtmlTab(tab, 2)).toMatch('<td>testC</td>');
});

test('Fonction de TEST n*3', () => {
    const tab = ['testA', 'testB', 'testD', '15846'];
    expect(UtilTab.toHtmlTab(tab, 2)).toMatch('<td>testA</td>');
    expect(UtilTab.toHtmlTab(tab, 2)).toMatch('<td>testB</td>');
    expect(UtilTab.toHtmlTab(tab, 2)).toMatch('<td>testD</td>');
    expect(UtilTab.toHtmlTab(tab, 2)).toMatch('<td>15846</td>');
});


